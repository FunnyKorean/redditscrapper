import logging

logger = logging

logger.basicConfig(filename='logs.log', filemode='a', level=logging.INFO,
                   format="%(levelname)s: %(message)s | %(asctime)s ")
