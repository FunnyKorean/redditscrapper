from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
import asyncio
import aiohttp

from logging_handler import logger
# from files import write_to_file
from id_generator import generate_id

# Necessary links
reddit_url = 'https://www.reddit.com'
user_url = 'https://www.reddit.com/user/'


async def get_post_data(data: dict) -> str | None:
    # Getting post information
    link = data['permalink']
    username = data['author']
    number_of_votes = data['score']
    number_of_comments = data['comment-count']
    post_date = data['created-timestamp']
    post_category = await get_category(link)
    try:
        # Request user information
        user_post_karma, user_comment_karma, user_cake_date = await get_user_data(username)
    except:
        logger.error(f'{username} not found for post {link}')
        return None

    else:
        final_result = f'{generate_id()};{link};{username};{user_cake_date};{user_post_karma};' \
                       f'{user_comment_karma};{post_date};{number_of_comments};{number_of_votes};{post_category}'
        return final_result


async def get_user_data(username: str) -> tuple:
    async with aiohttp.ClientSession() as session:
        async with session.get(f'{user_url}{username}') as response:
            # getting user information from user profile
            response_data = await response.text()
            user_bs = BeautifulSoup(response_data, 'html.parser')

            # Scrapping the user data
            user_post_comment = user_bs.find_all('span', {'data-testid': 'karma-number'})
            user_post_karma = user_post_comment[0].text.strip()
            user_comment_karma = user_post_comment[1].text.strip()
            user_cake_date = user_bs.find('time', {'data-testid': 'cake-day'}).text.strip()

            print(username, user_post_karma, user_comment_karma, user_cake_date)

            return user_post_karma, user_comment_karma, user_cake_date


async def get_category(link: str) -> str:
    async with aiohttp.ClientSession() as session:
        async with session.get(f'{reddit_url}{link}') as response:
            response_data = await response.text()
            category_bs = BeautifulSoup(response_data, 'html.parser')
            category_data = category_bs.find('div', {'id': '-post-rtjson-content'})
            if category_data:
                return category_data.get_text().strip()
            else:
                return ''


def batch_generator(data: list, n: int):
    temp = len(data)
    for i in range(0, temp, n):
        yield data[i: min(i + n, temp)]


async def scrape_posts(number_of_posts, category, period):
    posts_url = f'/r/popular/{category}/?t={period}'

    logger.info('Program has started...')

    # Selenium set up
    driver = webdriver.Chrome()
    driver.get(f'{reddit_url}{posts_url}')
    elem = driver.find_element(By.TAG_NAME, 'body')

    # Too many requests problem
    bs_check = BeautifulSoup(driver.page_source, 'html.parser')
    check_data = bs_check.find('h1')
    if check_data:
        if check_data.text == 'whoa there, pardner!':
            logger.error('Wait a few minutes and try again, too many requests')
            return

    posts_data = []
    final_data = []
    while len(final_data) <= number_of_posts:
        temp_len = len(posts_data)
        elem.send_keys(Keys.END)
        time.sleep(5)
        bs = BeautifulSoup(driver.page_source, 'html.parser')
        posts_data = bs.find_all('shreddit-post')
        if not len(posts_data) == temp_len:
            posts = posts_data[temp_len: len(posts_data)]
            for batch in batch_generator(posts, 14):
                async_list = []
                for post in batch:
                    task = asyncio.create_task(get_post_data(post))
                    async_list.append(task)
                result = await asyncio.gather(*async_list)
                result = list(filter(None, result))
                final_data.extend(result)
                time.sleep(5)

    try:
        print('finished')
        # write_to_file(final_data[:number_of_posts])
    except IOError:
        logger.error('IO Error')
    else:
        logger.info('Program successfully completed.')





