import asyncio
from argparse import ArgumentParser

from scrapper import scrape_posts

parser = ArgumentParser()
parser.add_argument('--posts', help='Number of posts', default=100, type=int)
parser.add_argument('--category', help='Post Category', default='top')
parser.add_argument('--period', help='Time period', default='month')
args = parser.parse_args()

asyncio.run(scrape_posts(args.posts, args.category, args.period))
